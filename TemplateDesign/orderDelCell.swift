//
//  orderDelCell.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 1/25/21.
//

import UIKit

class orderDelCell: UITableViewCell {

    @IBOutlet var tableCellOuter: UIView!

    @IBOutlet var cartProductImg: UIImageView!
    @IBOutlet var cartProName: UILabel!
    @IBOutlet var quantityLbl: UILabel!
    @IBOutlet var costLbl: UILabel!
    @IBOutlet weak var ingredients: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
