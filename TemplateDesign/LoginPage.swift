//
//  LoginPage.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 1/29/21.
//

import UIKit
import Alamofire

class LoginPage: UIViewController {

    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var guestmobilenumberview: UIView!
    
    @IBOutlet weak var guestcountrycodeTF: UITextField!
    @IBOutlet weak var guestmobileTF: UITextField!
    
    @IBOutlet weak var BlurView: UIView!
    @IBOutlet weak var otpView: UIView!
    @IBOutlet weak var OtpTF: UITextField!
    @IBOutlet weak var cardview: UIScrollView!
    @IBOutlet weak var backlogBtn: UIButton!

    
    var forgotusername = String()
    var forgotpassword = String()
    var guestmobile = String()
    var verifyCode = String()
    var getid = Int()
    var fromwhich = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            backlogBtn.isHidden = false
            backlogBtn.layer.cornerRadius = 8
        }else{
            
            backlogBtn.isHidden = true
        }
        
        guestmobilenumberview.isHidden = true
        BlurView.isHidden = true
        cardview.layer.cornerRadius = 20
        otpView.layer.cornerRadius = 20
        guestmobilenumberview.layer.cornerRadius = 20
        
        let nc = NotificationCenter.default
        
        nc.removeObserver(self, name: Notification.Name("Sidehide"), object: self)
    }
    
    @IBAction func backlogBtnClicked(_ sender: UIButton) {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "CategoryController") as! CategoryController
        self.navigationController?.pushViewController(home, animated: false)
    }
    

    @IBAction func logBtnClicked(_ sender: UIButton) {
        
        if usernameTF.text == nil || usernameTF.text == "UserName" || usernameTF.text == "" || usernameTF.text == " "{
            self.showSimpleAlert(messagess: "Enter username")
        }else if passwordTF.text == nil || passwordTF.text == "Password" || passwordTF.text == "" || passwordTF.text == " "{
            self.showSimpleAlert(messagess: "Enter password")
        }else{
            
            ERProgressHud.sharedInstance.show(withTitle: "Loading...")

            loginApi()
            
        }
    }

    @IBAction func SignupBtnClicked(_ sender: UIButton) {
        

        let signup = self.storyboard?.instantiateViewController(withIdentifier: "SignUpPage") as! SignUpPage
        self.navigationController?.pushViewController(signup, animated: true)
        
        
    }
    
    
    
    @IBAction func guestGetOtpClicked(_ sender: Any) {
        
        if guestcountrycodeTF.text == nil || guestcountrycodeTF.text == "" || guestcountrycodeTF.text == " " {

            showSimpleAlert(messagess: "Enter country code")
        }else if guestmobileTF.text == nil || guestmobileTF.text == "" || guestmobileTF.text == " " {

            showSimpleAlert(messagess: "Enter mobile number")
        }else{

        guestmobile = guestcountrycodeTF.text!
            + guestmobileTF.text!

         ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        GestUserApi()

        }
      
      //  self.BlurView.isHidden = false
        
    }
    
    
    @IBAction func guestloginviewCancleClicked(_ sender: Any) {
        
        
            
//            UIView.animate(withDuration: 0.5, delay: 0.2, options: UIView.AnimationOptions.curveEaseOut, animations: {
//                self.guestmobilenumberview.alpha = 0
//            }, completion: { finished in
//                self.guestmobilenumberview.isHidden = true
//            })
        
        UIView .transition(with: self.guestmobilenumberview, duration: 0.5, options: .transitionCurlUp,
                                       animations: {
                    self.guestmobilenumberview.isHidden = true

                    })
        
        
        
    }
    
    @IBAction func guestloginClicked(_ sender: Any) {
       
        
//        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCurlUp, animations: {
//
//            self.guestmobilenumberview.alpha = 1
//
//        }, completion: { finished in
//            self.guestmobilenumberview.isHidden = false
//        })

        UIView .transition(with: self.guestmobilenumberview, duration: 0.5, options: .transitionCurlDown,
                                       animations: {
                    self.guestmobilenumberview.isHidden = false

                    })
                
        guestcountrycodeTF.resignFirstResponder()
       
    }
    
    @IBAction func OtpSubmitClicked(_ sender: Any) {
        
        if OtpTF.text == nil || OtpTF.text == "" || OtpTF.text == " "{
            
            showSimpleAlert(messagess: "Enter OTP")
        }else{
        
        verifyCode = OtpTF.text!
       
         ERProgressHud.sharedInstance.show(withTitle: "Loading...")
       
            adminlogincheck()
        }
        
     //   self.performSegue(withIdentifier: "location", sender: self)

        
    }
    
    
    
    @IBAction func otpcancelClicked(_ sender: Any) {
        
        self.BlurView.isHidden = true

    }
   
    
    //MARK: Webservice Call

    
   func loginApi() {


       let urlString = GlobalClass.DevlopmentApi+"rest-auth/login/v1/"

    AF.request(urlString, method: .post, parameters: ["username": usernameTF.text as Any, "password": passwordTF.text as Any,"restaurant_id":GlobalClass.restaurantGlobalid],encoding: JSONEncoding.default, headers: nil).responseJSON {
       response in
         switch response.result {
                       case .success:
                           print(response)

                           if response.response?.statusCode == 200{
                            
                            let dict :NSDictionary = response.value! as! NSDictionary
                            // print(dict)
                               let status = dict.value(forKey: "id")
                            let tok = dict.value(forKey: "token")
                            
                            let defaults = UserDefaults.standard
                            
                            defaults.set(status, forKey: "custId")
                            defaults.set(tok, forKey: "custToken")
                            
                            defaults.set("loged", forKey: "Usertype")
                            
                            defaults.set("reguser", forKey: "Userlog")
                            GlobalClass.customertoken = tok as! String
                            
                            ERProgressHud.sharedInstance.hide()

                            
                            let rest = self.storyboard?.instantiateViewController(withIdentifier: "CategoryController") as! CategoryController
                            self.navigationController?.pushViewController(rest, animated: true)
                            
                            
                           }else{
                            
                              if response.response?.statusCode == 403{
                                
                                ERProgressHud.sharedInstance.hide()

                                let dict :NSDictionary = response.value! as! NSDictionary
                                
                               let msgtxt = dict.value(forKey: "msg") as! String
                                
                                if msgtxt == "You do not have access to restaurant." {
                                   
                                    let alert = UIAlertController(title: nil, message: "You do not have access to this restaurant. If you want to login for this restaurant please click on OK.",         preferredStyle: UIAlertController.Style.alert)

                                    alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.default, handler: { _ in
                                        //Cancel Action//
                                    }))
                                    alert.addAction(UIAlertAction(title: "OK",
                                                                  style: UIAlertAction.Style.default,
                                                                  handler: {(_: UIAlertAction!) in
                                                                    //Sign out action
                                                                    ERProgressHud.sharedInstance.show(withTitle: "Loading...")

                                                                    self.tokenapi()
                                                                     
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    alert.view.tintColor = UIColor.black
                                    
                                    
                                    
                                }else{
                                
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    
                                }
                                
                            }else if response.response?.statusCode == 401{
                                
                                ERProgressHud.sharedInstance.hide()

                                self.showSimpleAlert(messagess: "Username or password is incorrect")
                            }else{
                                
                                self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                               }
                            

                           }
                           
                           break
                       case .failure(let error):
                        ERProgressHud.sharedInstance.hide()
                        print(error.localizedDescription)
                        
                        let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                        
                        
                        let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                        
                        let msgrs = "URLSessionTask failed with error: The request timed out."
                        
                        if error.localizedDescription == msg {

                            self.showSimpleAlert(messagess:"No Internet Detected")

                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                            self.showSimpleAlert(messagess:"Slow Internet Detected")

                        }else{
                        
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                       }

                           print(error)
                       }
       }


    }
    
    
    func Getidapi(){
        
       
        let defaults = UserDefaults.standard
        
        let admintoken = defaults.object(forKey: "adminToken")as? String
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
        let urlString = GlobalClass.DevlopmentApi+"/user/?username=\(usernameTF.text!)"
        
           
        print(" categoryurl - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                
            ]
      

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                                print(response)

                                if response.response?.statusCode == 200{
                                 
                                    let dict : NSDictionary = response.value! as! NSDictionary
                                    
                                    let firstarr : NSArray = dict["results"]as! NSArray
                                    
                                    let firstdict : NSDictionary = firstarr[0]as! NSDictionary
                                    
                                    let firstid = firstdict.value(forKey: "id")
                                    
                                    print(firstid ?? 00)

                                    self.getid = firstid as! Int
                                    
                                    self.addidrestaurant()
                                    
                                }else{
                                    
                  if response.response?.statusCode == 401{
                                    
                    ERProgressHud.sharedInstance.hide()
                  }
                     if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }
                                   
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {

                            self.showSimpleAlert(messagess:"No internet connection")

                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                            self.showSimpleAlert(messagess:"Slow Internet Detected")

                                }else{
                                
                        self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
  
    func tokenapi(){

        let urlString = GlobalClass.DevlopmentApi + "rest-auth/login/v1/"

        AF.request(urlString, method: .post, parameters: ["username":GlobalClass.adminusername, "password":GlobalClass.adminpassword,"restaurant_id":"1"],encoding: JSONEncoding.default, headers: nil).responseJSON {
       response in
         switch response.result {
                       case .success:
                           print(response)

                           if response.response?.statusCode == 200{
                            
                            let dict :NSDictionary = response.value! as! NSDictionary
                            // print(dict)
                            
                            let tok = dict.value(forKey: "token")
                            
                            let defaults = UserDefaults.standard
                            
                            defaults.set(tok, forKey: "adminToken")
                            
                        //    self.signupApi()
                          
                            self.Getidapi()
                            
                           }else{
                            
                            if response.response?.statusCode == 401{
                                
                                ERProgressHud.sharedInstance.hide()
                               // self.sessionAlert()
                                
                            }else if response.response?.statusCode == 500{
                                
                                ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                
                               //  print(dict.value(forKey: "msg") as! String)
                            }else{
                                
                                self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                               }
                          
                           }
                           
                           break
                       case .failure(let error):
                        
                        ERProgressHud.sharedInstance.hide()
                        print(error.localizedDescription)
                        
                        let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                        
                        
                        let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                        
                        let msgrs = "URLSessionTask failed with error: The request timed out."
                        
                        
                        if error.localizedDescription == msg {
                            
                            self.showSimpleAlert(messagess:"No internet connection")
                            
                        }else if error.localizedDescription == msgr ||  error.localizedDescription == msgrs {
                            
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                        
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                        }

                           print(error)
                       }
       }


    }
    
   
    func addidrestaurant() {

        let defaults = UserDefaults.standard
        
        let admintoken = defaults.object(forKey: "adminToken")as? String
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
            
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                
            ]

        let urlString = GlobalClass.DevlopmentApi+"userrestaurant/"

        AF.request(urlString, method: .post, parameters: ["user":getid ,"restaurant":GlobalClass.restaurantGlobalid],encoding: JSONEncoding.default, headers: headers).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 201{
                             
                             ERProgressHud.sharedInstance.hide()

                                ERProgressHud.sharedInstance.show(withTitle: "Loading...")

                                self.loginApi()
                             
                            }else{
                             
                               if response.response?.statusCode == 403{
                                 
                                 ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                 
                                let msgtxt = dict.value(forKey: "msg") as! String
                                 
                                 if msgtxt == "You do not have access to restaurant." {
                                    
                                     let alert = UIAlertController(title: nil, message: "You do not have access to restaurant.You want to login for this restaurant please click on OK.",         preferredStyle: UIAlertController.Style.alert)

                                     alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.default, handler: { _ in
                                         //Cancel Action//
                                     }))
                                     alert.addAction(UIAlertAction(title: "OK",
                                                                   style: UIAlertAction.Style.default,
                                                                   handler: {(_: UIAlertAction!) in
                                                                     //Sign out action
                                                                     ERProgressHud.sharedInstance.show(withTitle: "Loading...")

                                                                     self.tokenapi()
                                                                      
                                     }))
                                     self.present(alert, animated: true, completion: nil)
                                     alert.view.tintColor = UIColor(rgb: 0xFE9300)
                                     
                                     
                                     
                                 }else{
                                 
                                 self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                     
                                 }
                                 
                             }else if response.response?.statusCode == 401{
                                 
                                 ERProgressHud.sharedInstance.hide()

                                 self.showSimpleAlert(messagess: "Username or password is incorrect")
                             }else{
                                 
                                 self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                }
                             

                            }
                            
                            break
                        case .failure(let error):
                         ERProgressHud.sharedInstance.hide()
                         print(error.localizedDescription)
                         
                         let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                         
                         
                         let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                         
                         let msgrs = "URLSessionTask failed with error: The request timed out."
                         
                         if error.localizedDescription == msg {

                             self.showSimpleAlert(messagess:"No Internet Detected")

                         }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                             self.showSimpleAlert(messagess:"Slow Internet Detected")

                         }else{
                         
                             self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                        }

                            print(error)
                        }
        }


     }
    
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
 
    func usenamereset()  {
        
        let defaults = UserDefaults.standard
        
        let admintoken = defaults.object(forKey: "adminToken")as? String
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    let urlString = GlobalClass.DevlopmentApi + "forgot/user/"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]

        AF.request(urlString, method: .post, parameters: ["email":forgotusername],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                
                                    ERProgressHud.sharedInstance.hide()

                                    let alert = UIAlertController(title:nil , message: "Username has been sent on email",         preferredStyle: UIAlertController.Style.alert)


                                    alert.addAction(UIAlertAction(title: "OK",
                                                                  style: UIAlertAction.Style.default,
                                                                  handler: {(_: UIAlertAction!) in



                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    alert.view.tintColor = UIColor.black
                                    
                                 
                                }else{
                                    
                                     if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else if response.response?.statusCode == 400{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    func forgotpassApi() {
        
        let defaults = UserDefaults.standard
        
        let admintoken = defaults.object(forKey: "adminToken")as? String
       
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    let urlString = GlobalClass.DevlopmentApi + "rest-auth/password/reset/v1/"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]

        AF.request(urlString, method: .post, parameters: ["username":forgotpassword],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                
                                    ERProgressHud.sharedInstance.hide()

                                    let alert = UIAlertController(title:nil , message: "Password reset email has been sent",         preferredStyle: UIAlertController.Style.alert)


                                    alert.addAction(UIAlertAction(title: "OK",
                                                                  style: UIAlertAction.Style.default,
                                                                  handler: {(_: UIAlertAction!) in



                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    alert.view.tintColor = UIColor.black
                                    
                                 
                                }else{
                                    
                                     if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else if response.response?.statusCode == 400{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    
    func GestUserApi() {


        let urlString = GlobalClass.DevlopmentApi+"send/code/"

         AF.request(urlString, method: .post, parameters: ["phone_number": guestmobile],encoding: JSONEncoding.default, headers: nil).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 200{
                       
                                ERProgressHud.sharedInstance.hide()

                                self.BlurView.isHidden = false

                                self.showSimpleAlert(messagess: "OTP sent successfully on your given number")
                             
                            }else{
                             
                              if response.response?.statusCode == 403{
                                 
                                 ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                 
                                 self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                             }else if response.response?.statusCode == 401{
                                 
                                 ERProgressHud.sharedInstance.hide()

                                 self.showSimpleAlert(messagess: "Username or password is incorrect")
                                
                             }else if response.response?.statusCode == 500{
                                
                                ERProgressHud.sharedInstance.hide()

                                self.showSimpleAlert(messagess: "\(self.guestmobile) is not a valid phone number.")
                               
                            }else{
                                 
                                 self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                }
                             

                            }
                            
                            break
                        case .failure(let error):
                         ERProgressHud.sharedInstance.hide()
                         print(error.localizedDescription)
                         
                         let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                         
                         
                         let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                         
                         let msgrs = "URLSessionTask failed with error: The request timed out."
                         
                         if error.localizedDescription == msg {
                             
                             self.showSimpleAlert(messagess:"No Internet Detected")
                             
                         }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                             
                             self.showSimpleAlert(messagess:"Slow Internet Detected")
                             
                         }else{
                         
                             self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                         }

                            print(error)
                        }
        }


     }
     
   
    func adminlogincheck(){

        let urlString = GlobalClass.DevlopmentApi + "rest-auth/login/v1/"

        AF.request(urlString, method: .post, parameters: ["username":GlobalClass.adminusername, "password":GlobalClass.adminpassword,"restaurant_id":"1"],encoding: JSONEncoding.default, headers: nil).responseJSON {
       response in
         switch response.result {
                       case .success:
                           print(response)

                           if response.response?.statusCode == 200{
                            
                            let dict :NSDictionary = response.value! as! NSDictionary
                            // print(dict)
                            
                            let tok = dict.value(forKey: "token")
                            
                            let defaults = UserDefaults.standard
                            
                            defaults.set(tok, forKey: "custToken")
                            
                            self.UserVerifyApi()
                          
                           }else{
                            
                            if response.response?.statusCode == 401{
                                
                                ERProgressHud.sharedInstance.hide()
                               // self.sessionAlert()
                                
                            }else if response.response?.statusCode == 500{
                                
                                ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                
                               //  print(dict.value(forKey: "msg") as! String)
                            }else{
                                
                                self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                               }
                          
                           }
                           
                           break
                       case .failure(let error):
                        
                        ERProgressHud.sharedInstance.hide()
                        print(error.localizedDescription)
                        
                        let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                        
                        
                        let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                        
                        let msgrs = "URLSessionTask failed with error: The request timed out."
                        
                        
                        if error.localizedDescription == msg {
                            
                            self.showSimpleAlert(messagess:"No internet connection")
                            
                        }else if error.localizedDescription == msgr ||  error.localizedDescription == msgrs {
                            
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                        
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                        }

                           print(error)
                       }
       }


    }
    
    func UserVerifyApi() {


        let urlString = GlobalClass.DevlopmentApi+"guest/verify/"

        AF.request(urlString, method: .post, parameters: ["phone_number": guestmobile,"verification_code":verifyCode],encoding: JSONEncoding.default, headers: nil).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 200{
                       
                                 
                                 let dict :NSDictionary = response.value! as! NSDictionary
                                 // print(dict)
                                    let status = dict.value(forKey: "id")
                                 let tok = dict.value(forKey: "token")
                                 
                                 let defaults = UserDefaults.standard
                                 
                                 defaults.set(status, forKey: "custId")
                                 defaults.set(tok, forKey: "custToken")
                                 
                                 defaults.set("loged", forKey: "Usertype")
                                defaults.set("guestuser", forKey: "Userlog")
                                GlobalClass.customertoken = tok as! String
                                 
                                 ERProgressHud.sharedInstance.hide()
   
                              //   self.performSegue(withIdentifier: "location", sender: self)
                                
                                let rest = self.storyboard?.instantiateViewController(withIdentifier: "CategoryController") as! CategoryController
                                self.navigationController?.pushViewController(rest, animated: true)

                            }else{
                             
                              if response.response?.statusCode == 403{
                                 
                                 ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                 
                                 self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                             }else if response.response?.statusCode == 401{
                                 
                                 ERProgressHud.sharedInstance.hide()

                                 self.showSimpleAlert(messagess: "Username or password is incorrect")
                             }else if response.response?.statusCode == 400{
                                
                                ERProgressHud.sharedInstance.hide()

                                let dict :NSDictionary = response.value! as! NSDictionary
                                
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                            }else{
                                 
                                 self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                }
                             

                            }
                            
                            break
                        case .failure(let error):
                         ERProgressHud.sharedInstance.hide()
                         print(error.localizedDescription)
                         
                         let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                         
                         
                         let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                         
                         let msgrs = "URLSessionTask failed with error: The request timed out."
                         
                         if error.localizedDescription == msg {
                             
                             self.showSimpleAlert(messagess:"No Internet Detected")
                             
                         }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                             
                             self.showSimpleAlert(messagess:"Slow Internet Detected")
                             
                         }else{
                         
                             self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                         }

                            print(error)
                        }
        }


     }
    
    
  
    @IBAction func forgotusernameClicked(_ sender: UIButton) {

        var answer = String()
        
        let ac = UIAlertController(title: "Username  Reset", message: nil, preferredStyle: .alert)
       
        ac.addTextField { (textField) in
            textField.placeholder = "Enter your email"
            textField.textAlignment = .center
        }
        
        let submitAction = UIAlertAction(title: "OK", style: .default) { [self, unowned ac] _ in
                 let textstr = ac.textFields![0]
                answer = textstr.text!
                
                if answer == ""{
                    
                    self.showSimpleAlert(messagess: "Enter email")
                }else{
                    fromwhich = "username"
                    forgotusername = answer.lowercased()
                    
                    ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                    adminlogincheckforforgot()
                    
                }
                
            }
        
        let CANCELAction = UIAlertAction(title: "CANCEL", style: .default) {  _ in
            
        }

            ac.addAction(CANCELAction)
            ac.addAction(submitAction)

            present(ac, animated: true)
        ac.view.tintColor = UIColor.black
        
    }
    
    
    @IBAction func forgotpasswordclicked(_ sender: UIButton) {
        
        var answer = String()
 
        let ac = UIAlertController(title: "Password  Reset", message: nil, preferredStyle: .alert)
       
        ac.addTextField { (textField) in
            textField.placeholder = "Enter your username"
            textField.textAlignment = .center
        }
        
            let submitAction = UIAlertAction(title: "OK", style: .default) { [unowned ac] _ in
                let textstr = ac.textFields![0]
               answer = textstr.text!
               
               if answer == ""{
                   
                   self.showSimpleAlert(messagess: "Enter username")
               }else{
                self.fromwhich = "password"
                self.forgotpassword = answer
                ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                self.forgotpassApi()
                   
               }
                
                
            }
        
        let CANCELAction = UIAlertAction(title: "CANCEL", style: .default) {  _ in
            
        }

            ac.addAction(CANCELAction)
            ac.addAction(submitAction)

            present(ac, animated: true)
        ac.view.tintColor = UIColor.black
        
    }
    
    
    func adminlogincheckforforgot(){

        let urlString = GlobalClass.DevlopmentApi + "rest-auth/login/v1/"

        AF.request(urlString, method: .post, parameters: ["username":GlobalClass.adminusername, "password":GlobalClass.adminpassword,"restaurant_id":"1"],encoding: JSONEncoding.default, headers: nil).responseJSON {
       response in
         switch response.result {
                       case .success:
                           print(response)

                           if response.response?.statusCode == 200{
                            
                            let dict :NSDictionary = response.value! as! NSDictionary
                            // print(dict)
                            
                            let tok = dict.value(forKey: "token")
                            
                            let defaults = UserDefaults.standard
                            
                            defaults.set(tok, forKey: "adminToken")
                            
                            if self.fromwhich == "username" {
                                self.usenamereset()
                            }else if self.fromwhich == "password"{
                                self.forgotpassApi()
                                
                            }
                          
                           }else{
                            
                            if response.response?.statusCode == 401{
                                
                                ERProgressHud.sharedInstance.hide()
                               // self.sessionAlert()
                                
                            }else if response.response?.statusCode == 500{
                                
                                ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                
                               //  print(dict.value(forKey: "msg") as! String)
                            }else{
                                
                                self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                               }
                          
                           }
                           
                           break
                       case .failure(let error):
                        
                        ERProgressHud.sharedInstance.hide()
                        print(error.localizedDescription)
                        
                        let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                        
                        
                        let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                        
                        let msgrs = "URLSessionTask failed with error: The request timed out."
                        
                        
                        if error.localizedDescription == msg {
                            
                            self.showSimpleAlert(messagess:"No internet connection")
                            
                        }else if error.localizedDescription == msgr ||  error.localizedDescription == msgrs {
                            
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                        
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                        }

                           print(error)
                       }
       }


    }
    
}
