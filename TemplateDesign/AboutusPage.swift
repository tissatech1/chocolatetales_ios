//
//  AboutusPage.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 1/29/21.
//

import UIKit
import Alamofire
import SDWebImage

class AboutusPage: UIViewController {

    @IBOutlet weak var cardview: UIView!
    @IBOutlet weak var abouTxtView: UITextView!
    @IBOutlet weak var aboutImg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
     //   cardview.layer.cornerRadius = 20
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        getrestaurantApi()
        
    }
    

    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)

    }
    
    
    func getrestaurantApi(){
        
        var admintoken = String()
        let defaults = UserDefaults.standard
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            admintoken = (defaults.object(forKey: "adminToken")as? String)!
        }else{
            admintoken = (defaults.object(forKey: "custToken")as? String)!
        }
        
        let autho = "token \(admintoken)"
        
        let urlString = GlobalClass.DevlopmentApi+"restaurant/\(GlobalClass.restaurantGlobalid)/"
        
           
        print(" categoryurl - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]
      

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                                print(response)

                                if response.response?.statusCode == 200{
                                    
                                    let dict :NSDictionary = response.value! as! NSDictionary
                                   
                                   let data  = dict["desc"]as? String
                                    
                                    print(data as Any)
//                                    self.abouTxtView.text! = data ?? "About Us"
                                    let strtag = data ?? "About Us"
                                    
                                //    let str = strtag.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)

                                    self.abouTxtView.text! = strtag
                                  
                                    var urlStr = String()
                                    if dict["restaurant_url"] is NSNull || dict["restaurant_url"] == nil{

                                        urlStr = ""

                                    }else{
                                        urlStr = dict["restaurant_url"] as! String
                                    }

                                    
                                    let url = URL(string: urlStr )

                                    self.aboutImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
                                    self.aboutImg.sd_setImage(with: url) { (image, error, cache, urls) in
                                        if (error != nil) {
                                            // Failed to load image
                                            self.aboutImg.image = UIImage(named: "aboutnoimage.png")
                                        } else {
                                            // Successful in loading image
                                            self.aboutImg.image = image
                                        }
                                    }
                                    
                                    
                                    
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    
                                }else{
                                    
                  if response.response?.statusCode == 401{
                                    
                    ERProgressHud.sharedInstance.hide()

                    self.SessionAlert()
                          
                                    
                                    }
                                    
                                    
                                    if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {

                            self.showSimpleAlert(messagess:"No internet connection")

                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                            self.showSimpleAlert(messagess:"Slow Internet Detected")

                                }else{
                                
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                       

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                      

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let LoginPage = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(LoginPage, animated: true)
                                        
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    

}
