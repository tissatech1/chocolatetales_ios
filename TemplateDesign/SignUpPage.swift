//
//  SignUpPage.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 1/29/21.
//

import UIKit
import Alamofire

class SignUpPage: UIViewController {

    @IBOutlet var salutTxt: UITextField!
    @IBOutlet var userNameTxt: UITextField!
    @IBOutlet var firstNameTxt: UITextField!
    @IBOutlet var lastNameTxt: UITextField!
    @IBOutlet var emailTxt: UITextField!
    @IBOutlet var codeTxt: UITextField!
    @IBOutlet var mobileNumTxt: UITextField!
    @IBOutlet var passwordTxt: UITextField!
    @IBOutlet var confpasswordTxt: UITextField!
    @IBOutlet var blurView: UIView!
    @IBOutlet weak var cardview: UIScrollView!

    
    var regid = Int()
    var usernameget = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true, animated: true)
        blurView.isHidden = true
        cardview.layer.cornerRadius = 20
        
    }
    
    @IBAction func loginBtnClicked(_ sender: UIButton) {
        

        self.navigationController?.popViewController(animated: true)

        
    }
    
    @IBAction func SubmitBtnClicked(_ sender: UIButton) {
    
        if salutTxt.text == "" ||  salutTxt.text == nil {
            
            showSimpleAlert(messagess: "Enter salutation")
            
        }else if userNameTxt.text == "" ||  userNameTxt.text == nil{
            
            showSimpleAlert(messagess: "Enter user name")

        }else if firstNameTxt.text == "" ||  firstNameTxt.text == nil{
            
            showSimpleAlert(messagess: "Enter first name")

        }else if lastNameTxt.text == "" ||  lastNameTxt.text == nil{
            
            showSimpleAlert(messagess: "Enter last name")

        }else if emailTxt.text == "" ||  emailTxt.text == nil{
            
            showSimpleAlert(messagess: "Enter email")

        }else if mobileNumTxt.text == "" ||  mobileNumTxt.text == nil{
            
            showSimpleAlert(messagess: "Enter mobile number")

        }else if passwordTxt.text == "" ||  passwordTxt.text == nil{
            
            showSimpleAlert(messagess: "Enter password")

        }else if passwordTxt.text!.count <= 8 {
            
            showSimpleAlert(messagess: "Password must be greater than 8 characters")

        }else if confpasswordTxt.text == "" || confpasswordTxt.text == nil{
            
            showSimpleAlert(messagess: "Enter confirm password")
        }else if passwordTxt.text! != confpasswordTxt.text!{
            
            showSimpleAlert(messagess: "Password and confirm password does not match")
        }else if emailTxt.text?.isValidEmail == false{
            
            showSimpleAlert(messagess: "Enter valid email")
        }else{
        
        
       ERProgressHud.sharedInstance.show(withTitle: "Loading...")
       adminlogincheck()
            
        }
        
    
    }

    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                    //    ERProgressHud.sharedInstance.hide()
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    @IBAction func salutBtnClicked(_ sender: UIButton) {
        
        blurView.isHidden = false

        
    }
    
    @IBAction func mrBtnClicked(_ sender: UIButton) {
        blurView.isHidden = true
        salutTxt.text = "Mr."
    }
    
    @IBAction func mrsBtnClicked(_ sender: UIButton) {
        blurView.isHidden = true
        salutTxt.text = "Mrs."
    }
    
    @IBAction func missBtnClicked(_ sender: UIButton) {
        blurView.isHidden = true
        salutTxt.text = "Miss."
       
    }
    
    @IBAction func msBtnclicked(_ sender: UIButton) {
        blurView.isHidden = true
        salutTxt.text = "Ms."
       
    }
    




//MARK: Admin Login

func adminlogincheck(){

    let urlString = GlobalClass.DevlopmentApi + "rest-auth/login/v1/"

    AF.request(urlString, method: .post, parameters: ["username":GlobalClass.adminusername, "password":GlobalClass.adminpassword,"restaurant_id":"1"],encoding: JSONEncoding.default, headers: nil).responseJSON {
   response in
     switch response.result {
                   case .success:
                       print(response)

                       if response.response?.statusCode == 200{
                        
                        let dict :NSDictionary = response.value! as! NSDictionary
                        // print(dict)
                        
                        let tok = dict.value(forKey: "token")
                        
                        let defaults = UserDefaults.standard
                        
                        defaults.set(tok, forKey: "adminToken")
                        
                        self.signupApi()
                      
                       }else{
                        
                        if response.response?.statusCode == 401{
                            
                            ERProgressHud.sharedInstance.hide()
                           // self.sessionAlert()
                            
                        }else if response.response?.statusCode == 500{
                            
                            ERProgressHud.sharedInstance.hide()

                             let dict :NSDictionary = response.value! as! NSDictionary
                            
                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                            
                           //  print(dict.value(forKey: "msg") as! String)
                        }else{
                            
                            self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                           }
                      
                       }
                       
                       break
                   case .failure(let error):
                    
                    ERProgressHud.sharedInstance.hide()
                    print(error.localizedDescription)
                    
                    let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                    
                    
                    let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                    
                    let msgrs = "URLSessionTask failed with error: The request timed out."
                    
                    
                    if error.localizedDescription == msg {
                        
                        self.showSimpleAlert(messagess:"No internet connection")
                        
                    }else if error.localizedDescription == msgr ||  error.localizedDescription == msgrs {
                        
                        self.showSimpleAlert(messagess:"Slow Internet Detected")
                                
                            }else{
                    
                        self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                    }

                       print(error)
                   }
   }


}


func signupApi()  {
    
    let defaults = UserDefaults.standard
    
    let admintoken = defaults.object(forKey: "adminToken")as? String
  
    
    let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
    
let urlString = GlobalClass.DevlopmentApi + "user/"
    
    print("category Url -\(urlString)")
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": autho
        ]

    AF.request(urlString, method: .post, parameters: ["username":userNameTxt.text!, "email":emailTxt.text!.lowercased(),"first_name":firstNameTxt.text!,"last_name":lastNameTxt.text!,"password":passwordTxt.text!,"verified":"N","restaurant_id":GlobalClass.restaurantGlobalid],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
        response in
          switch response.result {
                        case .success:
                           // print(response)

                            if response.response?.statusCode == 200{
                            
                                let dict :NSDictionary = response.value! as! NSDictionary
                                
                                print(dict)
                                
                                regid = dict["id"]as! Int
                                
                                ERProgressHud.sharedInstance.hide()
                                ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                                createcustomerApi()
                             
                            }else{
                                
                                if response.response?.statusCode == 400{
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    self.showSimpleAlert(messagess: "Your email id/username is already used in Insta App application for one of the restaurant. You can login to Insta App or register with new email id/username")
                                    
                                }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                   }
                            }
                            
                            break
                        case .failure(let error):

                            ERProgressHud.sharedInstance.hide()
                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            
                            if error.localizedDescription == msg {
                                
                                self.showSimpleAlert(messagess:"No internet connection")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
        }
        
  
        
    }




func createcustomerApi()  {
    
    
    let now = Date()

        let formatter = DateFormatter()

        formatter.timeZone = TimeZone.current

        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS" ///2020-10-19 12:05:10.625

        let dateString = formatter.string(from: now)
    
    print(dateString)
    
    let phnumber = codeTxt.text! + mobileNumTxt.text!
    
    
    let defaults = UserDefaults.standard
    
    let admintoken = defaults.object(forKey: "adminToken")as? String
  let customeridStr = String(regid)
    
    let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
    
let urlString = GlobalClass.DevlopmentApi + "customer/"
    
    print("category Url -\(urlString)")
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": autho,
            "user_id": customeridStr,
            "action": "customer"
        ]

    AF.request(urlString, method: .post, parameters: ["customer_id":regid, "last_access":dateString,"extra":"extra","salutation":salutTxt.text!,"phone_number":phnumber,"first_name":firstNameTxt.text!,"last_name":lastNameTxt.text!,"email":emailTxt.text!],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
        response in
          switch response.result {
                        case .success:
                           // print(response)

                            if response.response?.statusCode == 200{
                            
                                let dict :NSDictionary = response.value! as! NSDictionary
                                
                                print(dict)
                                
                                let alert = UIAlertController(title:"Registration has been done successfully" , message: "We sent you an email for account verification",         preferredStyle: UIAlertController.Style.alert)

                              
                                alert.addAction(UIAlertAction(title: "OK",
                                                              style: UIAlertAction.Style.default,
                                                              handler: {(_: UIAlertAction!) in
                                                                
                                    ERProgressHud.sharedInstance.hide()
                                                               

                            self.navigationController?.popViewController(animated: false)

                                }))
                                self.present(alert, animated: true, completion: nil)
                                alert.view.tintColor = UIColor.black
                             
                            }else{
                                
                                if response.response?.statusCode == 400{
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    
                                    self.showSimpleAlert(messagess: "User exist in system with email or username")
                                }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                   }
                          
                                
                            }
                            
                            break
                        case .failure(let error):

                            ERProgressHud.sharedInstance.hide()
                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            
                            if error.localizedDescription == msg {
                                
                                self.showSimpleAlert(messagess:"No internet connection")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
        }
        
  
        
    }

}

extension String {
    var isValidEmail: Bool {
        NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}").evaluate(with: self)
    }
}

extension UIColor {
   convenience init(red: Int, green: Int, blue: Int) {
       assert(red >= 0 && red <= 255, "Invalid red component")
       assert(green >= 0 && green <= 255, "Invalid green component")
       assert(blue >= 0 && blue <= 255, "Invalid blue component")

       self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
   }

   convenience init(rgb: Int) {
       self.init(
           red: (rgb >> 16) & 0xFF,
           green: (rgb >> 8) & 0xFF,
           blue: rgb & 0xFF
       )
   }
}
