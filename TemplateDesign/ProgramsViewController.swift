//
//  ProogramsViewController.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 17/06/21.
//

import UIKit
import SideMenu


class ProgramsViewController: UIViewController {
    
    var choice = Int()
    
    @IBOutlet var cartcountLbl: UILabel!
    
    @IBOutlet var corporateView: UIView!
    @IBOutlet var kidsView: UIView!
    @IBOutlet var homepagetabbar: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            
            cartcountLbl.text = "0"
            
        }else{
        
        cartcountLbl.text = UserDefaults.standard.object(forKey: "cartdatacount") as? String
            
        }
        
        homepagetabbar.layer.cornerRadius = 10
        homepagetabbar.layer.shadowColor = UIColor.gray.cgColor
        homepagetabbar.layer.shadowOpacity = 1
        homepagetabbar.layer.shadowOffset = .zero
        homepagetabbar.layer.shadowRadius = 3
        
        cartcountLbl.layer.cornerRadius = 6
        cartcountLbl.layer.borderWidth = 1
        cartcountLbl.layer.borderColor = UIColor(rgb: 0xFC4355).cgColor
        
        
        
        kidsView.layer.cornerRadius = 20
        kidsView.layer.shadowColor = UIColor.gray.cgColor
        kidsView.layer.shadowOpacity = 1
        kidsView.layer.shadowOffset = .zero
        kidsView.layer.shadowRadius = 3
        
        corporateView.layer.cornerRadius = 20
        corporateView.layer.shadowColor = UIColor.gray.cgColor
        corporateView.layer.shadowOpacity = 1
        corporateView.layer.shadowOffset = .zero
        corporateView.layer.shadowRadius = 3
        
    }
    

    
    @IBAction func backClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func corporateClicked(_ sender: Any) {
        
        let web = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        web.headerText = "Corporate program"
        web.url = "https://chocolatetales.ca/corporate/"
        self.navigationController?.pushViewController(web, animated: true)
    }
    
    @IBAction func kidsClicked(_ sender: Any) {

        let web = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        web.headerText = "Kids program"

        web.url = "https://chocolatetales.ca/kids/"
        self.navigationController?.pushViewController(web, animated: true)
    }
    
    
    @IBAction func catClicked(_ sender: Any) {
        let order = self.storyboard?.instantiateViewController(withIdentifier: "CategoryController") as! CategoryController
        self.navigationController?.pushViewController(order, animated: true)
    }
    
    
    @IBAction func cartClicked(_ sender: Any) {
        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            showSimpleAlert(messagess: "Please login")
        }else{
        
        let order = self.storyboard?.instantiateViewController(withIdentifier: "CartPage") as! CartPage
        self.navigationController?.pushViewController(order, animated: true)
        }
    }
    
    @IBAction func ordersClicked(_ sender: Any) {
        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            showSimpleAlert(messagess: "Please login")
        }else{
        
        let order = self.storyboard?.instantiateViewController(withIdentifier: "OrderPage") as! OrderPage
        self.navigationController?.pushViewController(order, animated: true)
        }
    }
    
    
    
    @IBAction func moreClicked(_ sender: Any) {
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
        
    }
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   //     ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
}
