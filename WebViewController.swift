//
//  WebViewController.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 16/06/21.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
    
    
    @IBOutlet var header: UILabel!
    
    var url = String()
    var headerText = String()

    @IBOutlet var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        header.text = self.headerText
        
        let web_url = URL(string:url)!
         let web_request = URLRequest(url: web_url)
         webView.load(web_request)
        
        
        
    }
    

    @IBAction func backClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    

}
